
# Canadaian Aboriginal Syllabic Converter

The function of this program is to falicitate the conversion
from syllabic to and from romanised aboriginal text.
'Syllabics' are a method of writing commonly used in Ojibwe,
Cree, and Inuktitut communities (among others).

Here are some examples of aboriginal (Ojibwe) words with the
romanised and syllabic spelling.

	Anii!                 ᐊᓃ!
	Boozhoo               ᐴᔔ
	Anishinaabemowin      ᐊᓂᔑᓈᐯᒧᐎᓐ

Since each language has many dialects, each with different
spelling standards and characters, a 'ruleset' can be easily
defined using json.

## Usage
Run main.py at the command line, or add it to your path, or
/usr/local/bin etc.  
  
By default, the program is interactive and transliterates from
a romanised string to syllabic string. This behavior can be modified
using these flags:

| Argument | Description |
| -------- | ----------- |
|-l        | Language. Specifies which directory in the 'Rules' directory to create process and preprocess rules. Default is 'East\_ojibwe'.|
|--stdin  | If true, program reads from stdin.|
|--to-rmn | If enabled, syllabics are converted to their romanized counterparts.|
|--to-syl | If enabled, roman letters are converted to their syllabic counterparts. Default.|

## Defining languges
Currently only a single Ojibwe dialect is supported by this program, but a new dialect
or language can easily be defined. One needs to simply copy the East\_ojibwe
directory (which is inside the Rules dir) and use the format for their own language.
The three files needed in every rules directory include preprocess\_rs.json, preprocess\_sr.json,
and which define the preprocessing procedures _to_ and _from_ syllabics
respectively, and finally the patterns.json for the final processing procedures. These
json files include strings that will be run through regular expressions come transliteration
time. More in-depth instructions will be released when this whole ordeal is finalised (it's
very much subject to change right now).

## Notes
While trasnsliterating from romanised text to syllabics should be quite accurate,
doing the reverse will not be! This is due to many letter groups, such as p and b, or 
sh and zh, being represented by a single syllabic character. Knowledge of which character
to transliterate to requires knowledge of the respctive language, something far from the
scope of this project.  
  
Examples:

    boozhoo            >   ᐴᔔ         >   pooshoo  
    pooshoo            >   ᐴᔔ         >   pooshoo
    anishinaabemowin   >   ᐊᓂᔑᓈᐯᒧᐎᓐ   >   anishiaapemowin

