#!/usr/bin/python3

import json
import re
import argparse
import sys
import re

def parse_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument("-l", help="""Language. Specifies which directory in the
            'Rules' directory to create process and preprocess rules. Default is
            'East_ojibwe'.""")

    parser.add_argument("--stdin", action="store_true", help="""If true, program
                        reads from stdin.""")

    parser.add_argument("--to-rmn", action="store_true", help="""If enabled, 
            syllabics are converted to their romanized counterparts.""")

    parser.add_argument("--to-syl", action="store_true", help="""If enabled, 
            roman letters are converted to their syllabic counterparts. Default.""")

    args = parser.parse_args()

    global language
    if args.l is not None:
        language = args.l
    else:
        language = "East_ojibwe"

    global mode
    mode = "to_syl"
    if args.to_rmn:
        mode = "to_rmn"
    elif args.to_syl:
        mode = "to_syl"
    
    global text
    text = ""
    if args.stdin:
        text = sys.stdin.read().lower()
    else:
        text = input("Enter text: ")
        text = text.lower()

def build_rules(rulesFile):
    rules = []
    dataFile = open(rulesFile, 'r')
    data = json.load(dataFile)
    for rmn, syl in data.items():
        rules.append([rmn, syl])
    dataFile.close()
    return rules

def to_syl(text, rules):
    '''
    Below code sorts rules array (which is generated from a "rules" 
    json config) by the length of the romanized string. This makes sure
    that "naa" is interpreted as a single group instead of "n", "a", and "a"
    being processed seperately, for example
    '''
    rules.sort(key = lambda s: len(s[0]), reverse = True)
    for key in range (0, len(rules)):
        text = re.sub(rules[key][0], rules[key][1], text)
    return text

def to_rmn(text, rules):
    rules.sort(key = lambda s: len(s[1]), reverse = True)
    for key in range (0, len(rules)):
        text = re.sub(rules[key][1], rules[key][0], text)
    return text

mode = "to_syl"
parse_arguments()

# Preprocessing
if mode == 'to_syl':
    rules = build_rules('Rules/' + language + '/preprocess_rs.json')
    text = to_syl(text, rules)
elif mode == 'to_rmn':
    rules = build_rules('Rules/' + language + '/preprocess_sr.json')
    text = to_syl(text, rules)

# Processing
rules = build_rules('Rules/' + language + '/patterns.json')

if mode == 'to_syl':
    print(to_syl(text, rules))
elif mode == 'to_rmn':
    print(to_rmn(text, rules))
